#!/bin/bash
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
echo "Rust Installation is complete.  Now installing a C Compiler (gcc)"
sudo apt install build-essential -y
sudo apt-get install manpages-dev
echo "Verifying gcc succesfully installed..."
gcc --version
echo "If text containing 'gcc' with OS and version numbers, installation was successful!"
echo "You must restart your terminal for Rust installation to complete and take effect"
sudo reboot -h now
echo "In case you\'re working with WSL (Windows Subsystem Linux), a reboot will not work. Will now exit the console.  Start WSL back up after WSL is exited"
sleep 3
exit

