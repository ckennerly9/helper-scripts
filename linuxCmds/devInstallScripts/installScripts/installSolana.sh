#!/bin/bash
sh -c "$(curl -sSfL https://release.solana.com/v1.8.2/install)"
echo "Confirm Solana was installed with 'solana --version' command."
echo "If it doesn't work, close console and open a new one.  If using WSL, you may need to exit WSL and start a new session"

