#!/bin/bash
sudo apt install libudev-dev
sudo apt install pkg-config
sudo apt install libssl-dev
cargo install --git https://github.com/project-serum/anchor anchor-cli --locked
